using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class HandAnimationEvents : MonoBehaviour
{
    public Bomb bomb;
    private Animator _animator;
    public bool _canPlay = true;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && _canPlay)
        {
            _animator.Play("bomb_jump");
            _canPlay = false;
        }
    }

    public void BombJump()
    {
        bomb.Jump();
        _canPlay = true;
    }
}
