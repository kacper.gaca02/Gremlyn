using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Ground Check")]
    public bool isGrounded;
    public LayerMask whatIsGround;
    private float _playerHeight = 2;
    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        //check if on ground
        isGrounded = Physics.Raycast(transform.position, Vector3.down, _playerHeight * .5f + .2f, whatIsGround);

        if (isGrounded) rb.drag = 10;
        else rb.drag = 0;
    }
}
