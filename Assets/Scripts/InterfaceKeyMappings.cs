using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceKeyMappings : MonoBehaviour
{
    public GameObject inventory;
    public PlayerCamera playerCamera;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            inventory.SetActive(!inventory.activeSelf);
            playerCamera.canMoveMouse = !inventory.activeSelf;
        }
    }
}
