using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class References : MonoBehaviour
{

    public static References Instance { get; private set; }

    public Inventory inventory;

    private void Awake()
    {
        Instance = this;
    }

}
