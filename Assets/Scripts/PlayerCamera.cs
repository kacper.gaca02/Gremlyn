using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public float sensX, sensY;

    public Transform orientation;

    float xRotation, yRotation;

    public bool canMoveMouse;

    private void Start()
    {
        //�eby kursor nie przeszkadza�
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        canMoveMouse = true;
    }

    private void Update()
    {
        MouseControls();
    }

    void MouseControls()
    {
        if (canMoveMouse)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            float mouseX = Input.GetAxisRaw("Mouse X") * Time.deltaTime * sensX;
            float mouseY = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * sensY;

            yRotation += mouseX;

            xRotation -= mouseY;

            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            transform.rotation = Quaternion.Euler(xRotation, yRotation, 0);//obracanie kamery 360 stopni
            orientation.rotation = Quaternion.Euler(0, yRotation, 0);//obracanie gracza w prawo i lewo razem z kamer�
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void EnableMouse()
    {
        canMoveMouse = false;
    }

    public void DisableMouse() 
    {
        canMoveMouse = true;
    }
}
