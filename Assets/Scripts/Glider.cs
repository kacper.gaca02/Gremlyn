using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Glider : MonoBehaviour
{
    [Header("Dev")]
    [SerializeField] InputField glideSpeedIF;
    [SerializeField] InputField upForceIF;

    [Header("Vabs")]
    public bool canGlide = false;
    public float glideSpeed;
    public float upForce;

    private Rigidbody _playerRB;
    private Player _player;

    public Camera camera;

    private void Awake()
    {
        _playerRB = GetComponent<Rigidbody>();
        _player = GetComponent<Player>();
    }

    private void Start()
    {
        StarterPlayerPrefs();
    }

    private void FixedUpdate()
    {
        Glide();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E)) canGlide = !canGlide;
        if (_player.isGrounded) canGlide = false;
    }

    public void Glide()
    {
        if (canGlide)
        {
            _playerRB.AddForce(camera.transform.forward * glideSpeed, ForceMode.Force);
            _playerRB.AddForce(Vector3.up * upForce, ForceMode.Force);
        }
    }

    private void StarterPlayerPrefs()
    {
        if (PlayerPrefs.HasKey("glideSpeed")) glideSpeed = PlayerPrefs.GetFloat("glideSpeed");
        else
        {
            PlayerPrefs.SetFloat("glideSpeed", 1);
            glideSpeed = 1;
        }
        if (PlayerPrefs.HasKey("upForce")) upForce = PlayerPrefs.GetFloat("upForce");
        else
        {
            PlayerPrefs.SetFloat("upForce", 1);
            upForce = 1;
        }
    }
    public void DevSettings()
    {
        glideSpeed = float.Parse(glideSpeedIF.text);
        PlayerPrefs.SetFloat("glideSpeed", float.Parse(glideSpeedIF.text));

        upForce = float.Parse((upForceIF.text).ToString());
        PlayerPrefs.SetFloat("upForce", float.Parse(upForceIF.text));
    }
}
