using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRange : MonoBehaviour
{
    public Bomb bomb;
    public Collider playerCollider;

    private void Start()
    {
        Physics.IgnoreCollision(GetComponent<Collider>(), playerCollider, true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Surface")) bomb.reachingSurface = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Surface")) bomb.reachingSurface = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Surface")) bomb.reachingSurface = false;
    }

}
