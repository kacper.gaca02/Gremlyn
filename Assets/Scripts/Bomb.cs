using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Bomb : MonoBehaviour
{
    public bool reachingSurface = false;
    /*
        [Header("Dev")]
        [SerializeField] InputField explosionForceIF;
        [SerializeField] InputField envExplosionForceIF;
        [SerializeField] InputField envExplosionRadiusIF;
    */

    [Header("Player explosion effect")]
    public float explopsionForceBonus;
    public float explosionForce;
    public bool readyToJump;

    [Header("Environment explosion effect")] //env -> environment
    public float envExplosionRadius;
    public float envExplosionForce;

    [Header("Miscelaneous")]
    [SerializeField] private Camera _mainCamera;
    [SerializeField] private Transform orientation;
    [SerializeField] private ParticleSystem particleSystemPerf;
    Rigidbody rb;
    Player player;
    private void Start()
    {
        player = GetComponent<Player>();
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        readyToJump = true;

        StarterPlayerPrefs();
        explopsionForceBonus = .1f;
    }


    public void Jump()
    {
        // Bomb acceleration when bouncing from ground or walls
        if (readyToJump && reachingSurface)
        {
            readyToJump = false;
            rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
            rb.AddForce(-_mainCamera.transform.forward * (explosionForce + explopsionForceBonus), ForceMode.Impulse);
            EnvironmentExplode();
            DoSmoke();
            Invoke("ResetJump", .25f);
        }
        // Bomb acceleration when bouncing mid air
        else if(readyToJump && !reachingSurface)
        {
            readyToJump = false;
            rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
            rb.AddForce(-_mainCamera.transform.forward * (explosionForce/3), ForceMode.Impulse);
            EnvironmentExplode();
            Invoke("ResetJump", .25f);
        }

    }

    void EnvironmentExplode()
    {
        // Find all colliders within the explosion radius
        Collider[] colliders = Physics.OverlapSphere(transform.position, envExplosionRadius);

        // Apply explosion force to each collider with a Rigidbody
        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody nearbyObjectRB = nearbyObject.GetComponent<Rigidbody>();
            if (nearbyObjectRB != null && nearbyObject.CompareTag("inDangerZone"))
            {
                // Calculate direction and apply explosion force
                Vector3 explosionDirection = nearbyObject.transform.position - transform.position;
                float distance = explosionDirection.magnitude;
                float relativeDistance = (envExplosionRadius - distance) / envExplosionRadius;
                float force = relativeDistance * envExplosionForce;
                nearbyObjectRB.AddForce(explosionDirection.normalized * force, ForceMode.Impulse);
            }
        }
    }

    public void DoSmoke()
    {
        ParticleSystem smoke = Instantiate(particleSystemPerf);

        smoke.transform.position = transform.position;
    }

    private void ResetJump()
    {
        readyToJump = true;
    }

    private void StarterPlayerPrefs()
    {
        if (PlayerPrefs.HasKey("explosionForce")) explosionForce = PlayerPrefs.GetFloat("explosionForce");
        else
        {
            PlayerPrefs.SetFloat("explosionForce", 10);
            explosionForce = 10;
        }
        if (PlayerPrefs.HasKey("envExplosionForce")) envExplosionForce = PlayerPrefs.GetFloat("envExplosionForce");
        else
        {
            PlayerPrefs.SetFloat("envExplosionForce", 5);
            envExplosionForce = 5;
        }
        if (PlayerPrefs.HasKey("envExplosionRadius")) envExplosionRadius = PlayerPrefs.GetFloat("envExplosionRadius");
        else
        {
            PlayerPrefs.SetFloat("envExplosionRadius", 1);
            envExplosionRadius = 1;
        }
    }
/*
    public void DevSettings()
    {
        explosionForce = float.Parse(explosionForceIF.text);
        PlayerPrefs.SetFloat("explosionForce", float.Parse(explosionForceIF.text));

        envExplosionForce = float.Parse((envExplosionForceIF.text).ToString());
        PlayerPrefs.SetFloat("envExplosionForce", float.Parse(envExplosionForceIF.text));

        envExplosionRadius = float.Parse((envExplosionRadiusIF.text).ToString());
        PlayerPrefs.SetFloat("envExplosionRadius", float.Parse(envExplosionRadiusIF.text));
    }
*/
}
