using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ThrowMenuBomb : MonoBehaviour
{
    public float speed;
    [SerializeField] private GameObject _bomb;
    [SerializeField] private GameObject _signInstance;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject bomba = Instantiate(_bomb);
            bomba.transform.position = transform.position + transform.TransformDirection(Vector3.forward);
            bomba.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward) * 15,ForceMode.Impulse);
        }
    }
}
