using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MenuBomb : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("MenuTable"))
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
