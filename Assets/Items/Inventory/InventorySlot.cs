using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public ItemInfo _itemInfo;
    private Button _button;
    public Inventory inventory;

    private void Start()
    {
        _button = transform.Find("Button").GetComponent<Button>();
        _button.onClick.AddListener(() => SlotInteraction());
        inventory = transform.parent.parent.GetComponent<Inventory>();
    }


    public void SlotInteraction()
    {
        Debug.Log($"Item price: {_itemInfo.goldWorth} gold");
        inventory.RemoveItem(_itemInfo);
        Destroy(gameObject);
    }
}


