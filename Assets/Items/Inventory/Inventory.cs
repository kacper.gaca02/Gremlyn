using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public int maxItems = 12;
    public float coins;
    public float scraps;
    public Text coinDisplay, scrapsDisplay;
    public GameObject inventorySlot;
    public List<ItemInfo> items = new();
    private List<GameObject> slots = new();

    public bool AddItem(ItemInfo itemToAdd)
    {
        // Adds a new item if the inventory has space
        if (items.Count < maxItems)
        {
            items.Add(itemToAdd);
            GameObject slot = Instantiate(inventorySlot, transform.Find("Content").transform);
            slots.Add(slot);
            slot.transform.Find("ItemImage").GetComponent<Image>().sprite = itemToAdd.inventorySprite;
            slot.GetComponent<InventorySlot>()._itemInfo = itemToAdd;
            return true;
        }

        Debug.Log("No space in the inventory");
        return false;
    }

    public void ClearInventory()
    {
        foreach (var slot in slots)
        {
            Destroy(slot.gameObject);
        }
        items.Clear();
    }

    public void RemoveItem(ItemInfo iteminfo)
    {
        foreach(Transform s in transform.Find("Content").transform)
        {
            if (s.gameObject.GetComponent<InventorySlot>()._itemInfo == iteminfo)
            {
                Destroy(s.gameObject);
                items.Remove(iteminfo);
                break;
            }
            Debug.Log("Baila Ella");
        }
    }

    public void AddMoney(float coins_)
    {
        this.coins += coins_;
    }

    public void RefreshDisplays()
    {
        coinDisplay.text = coins.ToString();
        scrapsDisplay.text = scraps.ToString();
    }
}
