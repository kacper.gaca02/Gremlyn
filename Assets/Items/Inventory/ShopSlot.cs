using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSlot : MonoBehaviour
{
    public ItemInfo itemInfo;
    private ShopKeeper shopkeeper;
    public Inventory inventory;
    private Button button;
    public Image selectedImage;

    private void Awake()
    {
        button = transform.Find("Button").GetComponent<Button>();
        shopkeeper = transform.parent.parent.parent.parent.GetComponent<ShopKeeper>();
        inventory = References.Instance.inventory;
        selectedImage = shopkeeper.transform.Find("MAINcanvas/selectedImage").GetComponent<Image>();
        button.onClick.AddListener(() => SlotInteraction());

        if (inventory != null)
        {
            Debug.Log("NOT NULL!");
        }
    }

    public void SlotInteraction() //klikniÍcie w slot z przedmiotem
    {
        selectedImage.sprite = itemInfo.inventorySprite;
        shopkeeper.itemToSell = itemInfo;
    }
}
