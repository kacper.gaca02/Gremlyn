using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectItems : MonoBehaviour
{
    public LayerMask layer;
    public bool canInteract;

    private void Awake()
    {
        canInteract = true;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && canInteract)
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layer))
            {
                IInteractable interactable = hit.transform.GetComponent<IInteractable>();
                if (interactable != null) interactable.Interact();
            }
        }
    }
}
