using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Item : MonoBehaviour, IInteractable
{
    public ItemInfo item;
    //public float itemWeight;
    public float itemGoldWorth;
    public float itemScrapWorth;
    public string itemName;

    public Inventory inventory;

    void Start()
    {
        //itemWeight = item.weight;
        itemGoldWorth = item.goldWorth;
        itemScrapWorth = item.scrapWorth;
        itemName = item.displayName;
    }

    public void Interact()
    {
        onPickedUp();
    }

    public void onPickedUp()
    {
        Debug.Log($"You picked up a/an {itemName} worth {itemGoldWorth} gold or {itemScrapWorth} scrap.");
        inventory.AddItem(item);
        Destroy(this.gameObject);
    }



}
