using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Scriptable Objects/Collectible Items", fileName = "ItemInfo")]
public class ItemInfo : ScriptableObject
{
    //public float weight;
    //public float state;
    [Header("Merchant trades")]
    public float goldWorth;
    public float scrapWorth;
    public ItemType itemType;

    [Header("Inventory")]
    public Sprite inventorySprite;
    public string displayName;

    //[Header("Misc")]
    //public GameObject worldObject;
}
public enum ItemType
{
    Magic,
    MagicNOT
}
