using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopKeeper : MonoBehaviour, IInteractable
{
    private Inventory inventory;
    public PlayerCamera playerCamera;
    public Camera SKCamera;
    public GameObject inShopkeeperEq;
    public GameObject eqSlot;
    public LayerMask layer;

    public Image selectedImage;
    public ItemInfo itemToSell;

    public void Awake()
    {
        inventory = References.Instance.inventory;
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(SKCamera.ScreenPointToRay(Input.mousePosition), out hit, 20, layer))
            {
                if (hit.transform.gameObject.name == "Exit")
                {
                    SKCamera.gameObject.SetActive(false);
                    playerCamera.DisableMouse();
                    RemoveEq();
                    Camera.main.gameObject.GetComponent<DetectItems>().canInteract = true;
                }
                if (hit.transform.gameObject.name == "SellAllButton")
                {
                    SellAllItems();
                    SKCamera.gameObject.SetActive(false);
                    playerCamera.DisableMouse();
                    RemoveEq();
                    Camera.main.gameObject.GetComponent<DetectItems>().canInteract = true;
                }
                if (hit.transform.gameObject.name == "SellButton")
                {
                    SellItem();
                }
            }
        }
    }

    public void Interact()
    {
        playerCamera.EnableMouse();
        SKCamera.gameObject.SetActive(true);
        Camera.main.gameObject.GetComponent<DetectItems>().canInteract = false;
        AddEq();
    }

    public void SellAllItems()
    {
        float cashToPay = 0;
        foreach (var item in inventory.items)
        {
            cashToPay += item.goldWorth;
        }
        Debug.Log($"Sold for {cashToPay} gold");
        inventory.ClearInventory();
        inventory.AddMoney(cashToPay);
        inventory.RefreshDisplays();
        cashToPay = 0;
    }

    public void SellItem()
    {
        if (itemToSell != null)
        {
            inventory.AddMoney(itemToSell.goldWorth);
            inventory.RemoveItem(itemToSell);
            inventory.RefreshDisplays();
            RemoveEq();
            AddEq();
            selectedImage.sprite = null;
            Debug.Log("SOLD?");
        }
        else { Debug.Log("NULL"); }
        itemToSell = null;
    }

    public void AddEq()
    {
        foreach (var item in inventory.items)
        {
            GameObject slot = Instantiate(eqSlot, inShopkeeperEq.transform);
            slot.transform.Find("ItemImage").GetComponent<Image>().sprite = item.inventorySprite;
            slot.GetComponent<ShopSlot>().itemInfo = item;
        }
    }

    public void RemoveEq() 
    {
        foreach(Transform obj in inShopkeeperEq.transform)
        {
            Destroy(obj.gameObject);
        }
    }

}
