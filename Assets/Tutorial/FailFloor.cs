using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailFloor : MonoBehaviour
{
    [SerializeField] GameObject _spawnPointObject;
    Vector3 _spawnPoint;

    private void Awake()
    {
        _spawnPoint = _spawnPointObject.transform.position;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player")) collision.transform.position = _spawnPoint;
    }
}
